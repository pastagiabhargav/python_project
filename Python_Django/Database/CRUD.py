import pymysql

mydb = pymysql.connect(host="localhost",user="root",passwd="",database="mydb")
mycurson = mydb.cursor()

def Data_insert():
    Firstname = input("Enter Firstname = ")
    Lastname = input("Enter Lastname = ")
    Subject = input("Enter Subject = ")
    Marks = input("Enter Marks = ")

    insert_query = "insert into table_studernt (Firstname,Lastname,Subject,Marks) values (%s,%s,%s,%s)"
    all_values = (Firstname,Lastname,Subject,Marks)

    id = mycurson.execute(insert_query,all_values)
    mydb.commit()

    if id:
        print("Successfully dada inserted")
    else:
        print("Something went wrong !! ")

def Data_fetch():

    Subject = input("Enter Subject = ")

    insert_query = "SELECT * FROM table_studernt WHERE Subject = %s"
    all_values = (Subject)

    mycurson.execute(insert_query,all_values)
    mydb.commit()
    all_data = mycurson.fetchall()
    print("---->All Data",all_data)

def Update_data():

    Firstname = input("Enter Firstname who can change marks = ")
    Marks = input("Enter New Marks = ")

    Update_query = "UPDATE table_studernt SET Marks = %s WHERE Firstname = %s"
    all_values = (Marks,Firstname)

    id = mycurson.execute(Update_query,all_values)
    mydb.commit()

    if id:
        print("Successfully dada Update")
    else:
        print("Something went wrong !! ")

def Delete_data():

    Firstname = input("Enter Firstname = ")

    delete_query = "DELETE FROM table_studernt WHERE Firstname = %s"
    all_values = (Firstname)

    id = mycurson.execute(delete_query,all_values)
    mydb.commit()

    if id:
        print("Successfully dada Delete")
    else:
        print("Something went wrong !! ")

menu = (

    """ 
        Press 1 for Data Insert
        Press 2 for Data Fetch
        Press 3 for Data Update
        Press 4 for Data Delete 
        """
)

print(menu)

ch = int(input("Enter your choice = "))
if ch == 1:
    Data_insert()

elif ch == 2:
    Data_fetch()

elif ch == 3:
    Update_data()

elif ch == 4:
    Delete_data()

else:
    print("Wrong Choice !! ")
